package service;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class ServiceTest {
	private Service service;
	private Storage storage;
	private Patient Jane;
	private Patient Finn;
	private Patient Hans;
	private Patient Ulla;
	private Patient Ib;
	Patient p;
	Laegemiddel l;

	LocalTime[] klokkeslet;
	double[] mængde;

	@Before
	public void setUp() {
		service = Service.getService();

		p = service.opretPatient("Lars", "239900", 50.0);
		l = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

	}

	@Test(expected = IllegalArgumentException.class)
	public void OpretOrdinationTC1() {
		service.opretDagligSkaevOrdination(LocalDate.of(2016, 1, 2), LocalDate.of(2016, 1, 1), p, l, null, null);

	}

	@Test
	public void OpretOrdinationTC2() {
		klokkeslet = new LocalTime[2];
		klokkeslet[0] = LocalTime.of(12, 00);
		klokkeslet[1] = LocalTime.of(14, 00);
		mængde = new double[2];
		mængde[0] = 2.0;
		mængde[1] = 3.0;

		DagligSkaev DS = service.opretDagligSkaevOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3), p, l,
				klokkeslet, mængde);

		assertEquals(1, p.getOrdinationer().size());
		assertEquals(l, DS.getLaegemiddel());
		assertEquals(LocalTime.of(12, 00), DS.getDoser().get(0).getTid());
		assertEquals(2.0, DS.getDoser().get(0).getAntal(), 0.0001);

	}

	@Test
	public void OpretOrdinationTC3() {
		klokkeslet = new LocalTime[2];
		klokkeslet[0] = LocalTime.of(12, 00);
		klokkeslet[1] = LocalTime.of(14, 00);
		mængde = new double[2];
		mængde[0] = 2.0;
		mængde[1] = 3.0;

		DagligSkaev DS = service.opretDagligSkaevOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 1), p, l,
				klokkeslet, mængde);

		assertEquals(1, p.getOrdinationer().size());
		assertEquals(l, DS.getLaegemiddel());
		assertEquals(LocalTime.of(12, 00), DS.getDoser().get(0).getTid());
		assertEquals(2.0, DS.getDoser().get(0).getAntal(), 0.0001);

	}

	@Test
	public void OpretOrdinationTC4() {
		klokkeslet = new LocalTime[1];
		klokkeslet[0] = LocalTime.of(12, 00);

		mængde = new double[1];
		mængde[0] = 2.0;

		DagligSkaev DS = service.opretDagligSkaevOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3), p, l,
				klokkeslet, mængde);

		assertEquals(1, p.getOrdinationer().size());
		assertEquals(l, DS.getLaegemiddel());
		assertEquals(LocalTime.of(12, 00), DS.getDoser().get(0).getTid());
		assertEquals(2.0, DS.getDoser().get(0).getAntal(), 0.0001);

	}

	@Test(expected = IllegalArgumentException.class)
	public void OpretOrdinationTC5() {
		klokkeslet = new LocalTime[1];
		klokkeslet[0] = LocalTime.of(12, 00);

		mængde = new double[1];
		mængde[0] = 2.0;

		DagligSkaev DS = service.opretDagligSkaevOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3), p, null,
				klokkeslet, mængde);

		assertEquals(1, p.getOrdinationer().size());
		assertEquals(l, DS.getLaegemiddel());
		assertEquals(LocalTime.of(12, 00), DS.getDoser().get(0).getTid());
		assertEquals(2.0, DS.getDoser().get(0).getAntal(), 0.0001);

	}

	@Test(expected = IllegalArgumentException.class)
	public void OpretOrdinationTC6() {
		klokkeslet = new LocalTime[1];
		klokkeslet[0] = LocalTime.of(12, 00);

		mængde = new double[1];
		mængde[0] = 2.0;

		DagligSkaev DS = service.opretDagligSkaevOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3), null, l,
				klokkeslet, mængde);

		assertEquals(1, p.getOrdinationer().size());
		assertEquals(l, DS.getLaegemiddel());
		assertEquals(LocalTime.of(12, 00), DS.getDoser().get(0).getTid());
		assertEquals(2.0, DS.getDoser().get(0).getAntal(), 0.0001);

	}

	@Test(expected = IllegalArgumentException.class)
	public void opretPnOrdinationTC1() {
		service.opretPNOrdination(LocalDate.of(2016, 1, 2), LocalDate.of(2016, 1, 1), p, l, 100);

	}

	@Test
	public void opretPnOrdinationTC2() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3), p, l, 100);
		assertEquals(1, p.getOrdinationer().size());
		assertEquals(l, pn.getLaegemiddel());
		assertEquals(100, pn.getAntalEnheder(), 0.0001);
	}

	@Test
	public void opretPnOrdinationTC3() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 1), p, l, 100);
		assertEquals(1, p.getOrdinationer().size());
		assertEquals(l, pn.getLaegemiddel());
		assertEquals(100, pn.getAntalEnheder(), 0.0001);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void opretPnOrdinationTC4() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3), p, l, 0);

	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void opretPnOrdinationTC5() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3), p, l, -1);

	}

	@Test(expected = IllegalArgumentException.class)
	public void opretPnOrdinationTC6() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3), p, null, 100);

	}

	@Test(expected = IllegalArgumentException.class)
	public void opretPnOrdinationTC7() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3), null, l, 100);

	}

	@Test(expected = IllegalArgumentException.class)
	public void opretDagligFastTC1() {

		DagligFast DF = service.opretDagligFastOrdination(LocalDate.of(2016, 1, 2), LocalDate.of(2016, 1, 1), p, l, 1,
				1, 1, 1);

	}

	@Test
	public void opretDagligFastTC2() {
		DagligFast DF = service.opretDagligFastOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3), p, l, 1,
				1, 1, 1);
		assertEquals(1, p.getOrdinationer().size());
		assertEquals(l, DF.getLaegemiddel());
		assertEquals(1.0, DF.getDoser()[0].getAntal(), 0.0001);
	}

	@Test
	public void opretDagligFastTC3() {
		DagligFast DF = service.opretDagligFastOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 1), p, l, 1,
				1, 1, 1);
		assertEquals(1, p.getOrdinationer().size());
		assertEquals(l, DF.getLaegemiddel());
		assertEquals(1.0, DF.getDoser()[0].getAntal(), 0.0001);
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretDagligFastTC4() {
		DagligFast DF = service.opretDagligFastOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 2), p, l, -1,
				1, 1, 1);
	}

	@Test
	public void opretDagligFastTC5() {
		DagligFast DF = service.opretDagligFastOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 2), p, l, 0,
				1, 1, 1);
		assertEquals(1, p.getOrdinationer().size());
		assertEquals(l, DF.getLaegemiddel());
		assertEquals(0, DF.getDoser()[0].getAntal(), 0.0001);
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretDagligFastTC6() {
		DagligFast DF = service.opretDagligFastOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 2), p, null,
				-1, 1, 1, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretDagligFastTC7() {
		DagligFast DF = service.opretDagligFastOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 2), null, l,
				-1, 1, 1, 1);
	}

	@Test
	public void ordinationPNAnvendtTC1() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7), p, l, 100);
		assertFalse(service.ordinationPNAnvendt(pn, LocalDate.of(2015, 12, 29)));
	}

	@Test
	public void ordinationPNAnvendtTC2() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7), p, l, 100);

		assertTrue(service.ordinationPNAnvendt(pn, LocalDate.of(2016, 1, 1)));
	}

	@Test
	public void ordinationPNAnvendtTC3() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7), p, l, 100);
		assertTrue(service.ordinationPNAnvendt(pn, LocalDate.of(2016, 1, 1)));
	}

	@Test
	public void ordinationPNAnvendtTC4() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7), p, l, 100);
		assertTrue(service.ordinationPNAnvendt(pn, LocalDate.of(2016, 1, 7)));
	}

	@Test
	public void ordinationPNAnvendtTC5() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7), p, l, 100);
		assertFalse(service.ordinationPNAnvendt(pn, LocalDate.of(2016, 1, 8)));
	}

	@Test
	public void AnbefaletDosisPrDøgnTC1() {
		Patient p1 = service.opretPatient("Lars", "2395801234", 15);

		assertEquals(15, service.anbefaletDosisPrDoegn(p1, l), 0.0001);
	}

	@Test
	public void AnbefaletDosisPrDøgnTC2() {
		Patient p1 = service.opretPatient("Lars", "2395801234", 25);

		assertEquals(37.5, service.anbefaletDosisPrDoegn(p1, l), 0.0001);
	}

	@Test
	public void AnbefaletDosisPrDøgnTC3() {
		Patient p1 = service.opretPatient("Lars", "2395801234", 50);

		assertEquals(75, service.anbefaletDosisPrDoegn(p1, l), 0.0001);
	}

	@Test
	public void AnbefaletDosisPrDøgnTC4() {
		Patient p1 = service.opretPatient("Lars", "2395801234", 120);

		assertEquals(180, service.anbefaletDosisPrDoegn(p1, l), 0.0001);
	}

	@Test
	public void AnbefaletDosisPrDøgnTC5() {
		Patient p1 = service.opretPatient("Lars", "2395801234", 125);

		assertEquals(250, service.anbefaletDosisPrDoegn(p1, l), 0.0001);
	}

	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC1() {
		createSomeObjects();
		assertEquals(0, service.antalOrdinationerPrVægtPrLægemiddel(0, 25, l));
	}

	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC2() {
		createSomeObjects();
		assertEquals(1, service.antalOrdinationerPrVægtPrLægemiddel(20, 80, l));
	}
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC3() {
		createSomeObjects();
		assertEquals(3, service.antalOrdinationerPrVægtPrLægemiddel(80, 120, l));
	}
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC4() {
		createSomeObjects();
		assertEquals(0, service.antalOrdinationerPrVægtPrLægemiddel(120, 160, l));
	}
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC5() {
		createSomeObjects();
		assertEquals(0, service.antalOrdinationerPrVægtPrLægemiddel(60, 10, l));
	}


	public void createSomeObjects() {
		Jane = service.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Finn = service.opretPatient("Finn Madsen", "070985-1153", 83.2);
		Hans = service.opretPatient("Hans Jørgensen", "050972-1233", 89.4);
		Ulla = service.opretPatient("Ulla Nielsen", "011064-1522", 59.9);
		Ib = service.opretPatient("Ib Hansen", "090149-2529", 87.7);

		Laegemiddel l1 = service.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		l = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		Laegemiddel l2 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		Laegemiddel l3 = service.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), Finn, l, 123);

		service.opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14), Jane, l1, 3);

		service.opretPNOrdination(LocalDate.of(2015, 1, 20), LocalDate.of(2015, 1, 25), Ulla, l2, 5);

		service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), Jane, l, 123);

		service.opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12), Finn, l, 2, 0, 1, 0);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		service.opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24),
				Finn, l, kl, an);
	}
}
