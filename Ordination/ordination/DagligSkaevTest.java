package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class DagligSkaevTest {

	@Test
	public void testSamletDosisTC1() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 1));
		DS.opretDosis(LocalTime.of(6, 20), 200);

		assertEquals(200, DS.samletDosis(), 0.0001);
	}

	@Test
	public void testSamletDosisTC2() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 1));
		DS.opretDosis(LocalTime.of(6, 20), 200);
		DS.opretDosis(LocalTime.of(7, 20), 200);
		DS.opretDosis(LocalTime.of(8, 20), 200);
		DS.opretDosis(LocalTime.of(9, 20), 200);

		assertEquals(800, DS.samletDosis(), 0.0001);
	}

	@Test
	public void testSamletDosisTC3() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3));
		DS.opretDosis(LocalTime.of(6, 20), 200);
		DS.opretDosis(LocalTime.of(7, 20), 200);
		DS.opretDosis(LocalTime.of(8, 20), 200);
		DS.opretDosis(LocalTime.of(9, 20), 200);
		DS.opretDosis(LocalTime.of(10, 20), 200);
		DS.opretDosis(LocalTime.of(11, 20), 200);
		DS.opretDosis(LocalTime.of(12, 20), 200);
		DS.opretDosis(LocalTime.of(13, 20), 200);
		DS.opretDosis(LocalTime.of(14, 20), 200);
		DS.opretDosis(LocalTime.of(15, 20), 200);
		DS.opretDosis(LocalTime.of(16, 20), 200);
		DS.opretDosis(LocalTime.of(17, 20), 200);

		assertEquals(2400, DS.samletDosis(), 0.0001);
	}

	@Test
	public void testSamletDosisTC4() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3));
		DS.opretDosis(LocalTime.of(6, 20), 200);
		DS.opretDosis(LocalTime.of(7, 20), 200);
		DS.opretDosis(LocalTime.of(8, 20), 200);

		assertEquals(600, DS.samletDosis(), 0.0001);
	}

	@Test
	public void testDoegnDosisTC1() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 1));
		DS.opretDosis(LocalTime.of(6, 20), 200);

		assertEquals(200, DS.samletDosis(), 0.0001);
	}

	@Test
	public void testDoegnDosisTC2() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 1));
		DS.opretDosis(LocalTime.of(6, 20), 200);
		DS.opretDosis(LocalTime.of(7, 20), 200);
		DS.opretDosis(LocalTime.of(8, 20), 200);
		DS.opretDosis(LocalTime.of(9, 20), 200);

		assertEquals(800, DS.samletDosis(), 0.0001);
	}

	@Test
	public void testDoegnDosisTC3() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3));

		DS.opretDosis(LocalTime.of(6, 20), 200);
		DS.opretDosis(LocalTime.of(7, 20), 200);
		DS.opretDosis(LocalTime.of(8, 20), 200);
		DS.opretDosis(LocalTime.of(9, 20), 200);

		assertEquals(800, DS.samletDosis(), 0.0001);
	}

	@Test
	public void testDoegnDosisTC4() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 3));
		DS.opretDosis(LocalTime.of(6, 20), 200);

		assertEquals(200, DS.samletDosis(), 0.0001);
	}
}
