package ordination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class PNTest {
	PN pn;

	@Before
	public void setUp() throws Exception {
		pn = new PN(LocalDate.of(2016, 1, 2), LocalDate.of(2016, 1, 4));

	}

	@Test
	public void testGivDosisTC1() {
		assertFalse(pn.givDosis(LocalDate.of(2016, 1, 1)));
	}

	@Test
	public void testGivDosisTC2() {
		assertTrue(pn.givDosis(LocalDate.of(2016, 1, 3)));
		assertEquals(1, pn.getAntalGangeGivet());
	}

	@Test
	public void testGivDosisTC3() {
		assertTrue(pn.givDosis(LocalDate.of(2016, 1, 3)));
		assertEquals(1, pn.getAntalGangeGivet());
	}

	@Test
	public void testGivDosisTC4() {
		assertTrue(pn.givDosis(LocalDate.of(2016, 1, 4)));
		assertEquals(1, pn.getAntalGangeGivet());
	}

	@Test
	public void testGivDosisTC5() {
		assertFalse(pn.givDosis(LocalDate.of(2016, 1, 5)));
	}

	@Test
	public void testDøgnDosisTC1() {
		pn = new PN(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 1));
		pn.setAntalEnheder(200);
		pn.givDosis(LocalDate.of(2016, 1, 1));
		assertEquals(200, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDøgnDosisTC2() {
		pn = new PN(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 2));
		pn.setAntalEnheder(200);
		pn.givDosis(LocalDate.of(2016, 1, 1));
		pn.givDosis(LocalDate.of(2016, 1, 2));
		assertEquals(200, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDøgnDosisTC3() {
		pn = new PN(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 4));
		pn.setAntalEnheder(200);
		pn.givDosis(LocalDate.of(2016, 1, 1));
		pn.givDosis(LocalDate.of(2016, 1, 4));
		assertEquals(100, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDøgnDosisTC4() {
		pn = new PN(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 1));
		pn.setAntalEnheder(200);
		pn.givDosis(LocalDate.of(2016, 1, 1));
		pn.givDosis(LocalDate.of(2016, 1, 1));
		pn.givDosis(LocalDate.of(2016, 1, 1));
		pn.givDosis(LocalDate.of(2016, 1, 1));
		assertEquals(800, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDøgnDosisTC5() {
		pn = new PN(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 2));
		pn.setAntalEnheder(200);
		pn.givDosis(LocalDate.of(2016, 1, 1));
		pn.givDosis(LocalDate.of(2016, 1, 1));
		pn.givDosis(LocalDate.of(2016, 1, 1));
		pn.givDosis(LocalDate.of(2016, 1, 2));
		assertEquals(400, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testSamletDosisTC1() {
		pn.setAntalEnheder(100);
		assertEquals(0, pn.samletDosis(), 0.001);
	}

	@Test
	public void testSamletDosisTC2() {
		pn.setAntalEnheder(100);
		pn.givDosis(LocalDate.of(2016, 1, 2));
		assertEquals(100, pn.samletDosis(), 0.001);
	}

	@Test
	public void testSamletDosisTC3() {
		pn.setAntalEnheder(100);
		pn.givDosis(LocalDate.of(2016, 1, 2));
		pn.givDosis(LocalDate.of(2016, 1, 2));
		pn.givDosis(LocalDate.of(2016, 1, 2));
		pn.givDosis(LocalDate.of(2016, 1, 2));
		pn.givDosis(LocalDate.of(2016, 1, 2));
		assertEquals(500, pn.samletDosis(), 0.001);
	}

}
