package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> dosiser;

	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
		dosiser = new ArrayList<>();
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis d = new Dosis(tid, antal);
		dosiser.add(d);
	}

	@Override
	public double samletDosis() {
		int sum = 0;

		for (Dosis dosis : dosiser) {
			sum += dosis.getAntal();
		}
		return sum;
	}

	@Override
	public double doegnDosis() {

		return samletDosis() / antalDage();
	}

	@Override
	public String getType() {

		return "Daglig skæv. ";
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(dosiser);
	}
}
