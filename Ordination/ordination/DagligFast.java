package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private Dosis[] dosiser;

	public DagligFast(LocalDate startDen, LocalDate slutDen, double morgenAntal, double middagAntal, double aftenAntal,
			double natAntal) {
		super(startDen, slutDen);
		dosiser = new Dosis[4];
		for (int i = 0; i < dosiser.length; i++) {
			if (morgenAntal < 0 || middagAntal < 0 || aftenAntal < 0
					|| natAntal < 0) {
				throw new IllegalArgumentException("Dosis skal være et positivt tal");
			} else {
				dosiser[0] = new Dosis(LocalTime.of(6, 0), morgenAntal);
				dosiser[1] = new Dosis(LocalTime.of(12, 0), middagAntal);
				dosiser[2] = new Dosis(LocalTime.of(18, 0), aftenAntal);
				dosiser[3] = new Dosis(LocalTime.of(00, 0), natAntal);
			}
		}
		
	}

	@Override
	public double samletDosis() {
		
			double sum = 0;
			for (int d = 0; d < dosiser.length; d++) {
				sum += dosiser[d].getAntal();
			}
			sum = sum * antalDage();

			return sum;
		
	}

	@Override
	public double doegnDosis() {
			double sum = 0;
			for (int d = 0; d < 4; d++) {
				sum += dosiser[d].getAntal();
			}
			return sum;
		
	}

	@Override
	public String getType() {

		return "Daglig fast";
	}

	public Dosis[] getDoser() {
		return dosiser;
	}

}
