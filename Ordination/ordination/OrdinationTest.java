package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Test;

public class OrdinationTest {

	@Test
	public void testAntalDageTC1() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 2), LocalDate.of(2016, 1, 1));
		assertEquals(0, DS.antalDage());
	}

	@Test
	public void testAntalDageTC2() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 1));
		assertEquals(1, DS.antalDage());
	}
	@Test
	public void testAntalDageTC3() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 2));
		assertEquals(2, DS.antalDage());
	}
	

}
