package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private ArrayList<LocalDate> dosisDatoer;

	public PN(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);

		dosisDatoer = new ArrayList<>();
	}

	private double antalEnheder;

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {

		if (givesDen.isAfter(getStartDen().minusDays(1)) && givesDen.isBefore(getSlutDen().plusDays(1))) {
			dosisDatoer.add(givesDen);
			return true;
		}
		return false;
	}

	@Override
	public double doegnDosis() {
		if (dosisDatoer.size() > 0) {
			return samletDosis()
					/ (ChronoUnit.DAYS.between(dosisDatoer.get(0), dosisDatoer.get(dosisDatoer.size() - 1)) + 1);
		} else {
			return 0;
		}

	}

	@Override
	public double samletDosis() {
		return antalEnheder * dosisDatoer.size();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {

		return dosisDatoer.size();
	}

	public void setAntalEnheder(double e) {

		if (e <= 0) {
			throw new IndexOutOfBoundsException();

		} else {
			antalEnheder = e;
		}

	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {

		return "PN type. ";
	}

}
