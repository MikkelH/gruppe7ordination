package ordination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;

import org.junit.Test;

public class DagligFastTest {

	private DagligFast df;

	@Test
	public void samletDosisTest1() {
		df = new DagligFast(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 01), 0, 1, 1, 1);
		assertEquals(3, df.samletDosis(), 0);
	}


	@Test
	public void samletDosisTest3() {
		df = new DagligFast(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 01), 5, 1, 1, 1);
		assertEquals(8, df.samletDosis(), 0);
	}

	@Test
	public void samletDosisTest4() {
		df = new DagligFast(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 02), 1, 1, 1, 1);
		assertEquals(8, df.samletDosis(), 0);
	}

	@Test
	public void doegnDosisTest1() {
		df = new DagligFast(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 01), 0, 1, 1, 1);
		assertEquals(3, df.doegnDosis(), 0);
	}

	@Test
	public void doegnDosisTest3() {
		df = new DagligFast(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 01), 5, 1, 1, 1);
		assertEquals(8, df.doegnDosis(), 0);
	}

	@Test
	public void doegnDosisTest4() {
		df = new DagligFast(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 02), 1, 1, 1, 1);
		assertEquals(4, df.doegnDosis(), 0);
	}

	@Test
	public void doegnDosisTest5() {
		df = new DagligFast(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 10), 1, 1, 1, 1);
		assertEquals(4, df.doegnDosis(), 0);
	}

}
